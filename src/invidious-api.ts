/* This file is part of PeerTubeify.
 *
 * PeerTubeify is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * PeerTubeify is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * PeerTubeify. If not, see <https://www.gnu.org/licenses/>.
 */

import * as _ from 'lodash/fp';
import constants from './constants';

async function fetchAPI(action: string, params: any) {
    const paramString = typeof params == 'string'
        ? params
        : Object.keys(params).map(function(key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(params[key]);
        }).join('&');

    return fetch(`${constants.invidiousAPI.url}/${action}/${paramString}`)
        .then(res => res.json())
        .catch(e => console.error(
            'An error occured while trying to fetch Invidious API used by PeerTubeify: '
            + e.message
        ));
}


export async function getVideo(id: string) {
    return fetchAPI(constants.invidiousAPI.videos, id);
}

export async function getChannel(ucid: string) {
    return fetchAPI(constants.invidiousAPI.channels, ucid);
}
