export default {
    peertubeAPI: {
        defaultInstance: 'peertube.social',
        endpoint: 'api/v1',
    },
    invidiousAPI: {
        url: 'https://invidious.snopyta.org/api/v1',
        videos: 'videos',
        channels: 'channels'
    }
};
